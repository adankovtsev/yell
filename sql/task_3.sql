SELECT y.`type`, y.value
FROM yell_goods y
INNER JOIN (
	SELECT yg.type, MAX(yg.date) AS date
	FROM yell_goods yg
	GROUP BY yg.type
	ORDER BY NULL
) AS yd ON yd.type = y.type AND yd.date = y.date