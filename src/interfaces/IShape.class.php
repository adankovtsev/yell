<?php

namespace Yell\Interfaces;

interface IShape {
    public function draw();
    public function import(array $data);
}