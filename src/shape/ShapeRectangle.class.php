<?php

namespace Yell\Shape;

class ShapeRectangle extends AbstractShape
{
    public function getAttributes()
    {
        return array(
            'x' => 'Координата X',
            'y' => 'Координата Y',
            'height' => 'Высота',
            'width' => 'Ширина'
        );
    }

    public function draw()
    {
        return 'Rectangle (x:' . $this->x . '; y:' . $this->y . '; height:' . $this->height . '; width: ' . $this->width . ');';
    }
}