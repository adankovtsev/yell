<?php

namespace Yell\Shape;

class ShapeNull extends AbstractShape
{
    public function getAttributes()
    {
        return array('type' => 'Тип');
    }

    public function draw()
    {
        return 'Null object: Invalid object ' . $this->type;
    }
}