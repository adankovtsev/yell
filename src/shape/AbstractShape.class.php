<?php

namespace Yell\Shape;

use Yell\Interfaces\IShape;

abstract class AbstractShape implements IShape
{
    public $parameters;

    public static function create()
    {
        return new static();
    }

    public function getAttributes()
    {
        return array();
    }

    public function __get($name)
    {
        if (!isset($this->parameters[$name])) return null;
        return $this->parameters[$name];
    }

    public function __set($name, $value)
    {
        $this->parameters[$name] = $value;
    }

    public function import(array $data)
    {
        foreach ($this->getAttributes() as $key => $title) {
            $this->parameters[$key] = (isset($data[$key]) ? $data[$key] : null);
        }

        return $this;
    }
}