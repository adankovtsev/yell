<?php

namespace Yell\Shape;

class ShapeCircle extends AbstractShape
{
    public function getAttributes()
    {
        return array(
            'x' => 'Точка X',
            'y' => 'Точка Y',
            'radius' => 'Радиус',
            'width' => 'Толщина'
        );
    }

    public function draw()
    {
        return "Circle (x:{$this->x}; y:{$this->y}; radius: {$this->radius}; width:{$this->width})";
    }
}