<?php

use Yell\Classes\ShapePool;
/** @var ShapePool $pool */
?>
<h3>Фигуры:</h3>
<ul>
    <?php foreach ($pool as $shape): ?>
        <li><?php echo $shape->draw(); ?></li>
    <?php endforeach; ?>
</ul>
