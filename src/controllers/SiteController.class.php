<?php

namespace Yell\Controllers;

use Yell\Classes\Controller;
use Yell\Classes\ShapeFactory;
use Yell\Interfaces\IShape;
use Yell\Classes\ShapePool;
use Yell\Exceptions\YellClassNotFoundException;

class SiteController extends Controller
{
    private function makeShape(array $row)
    {
        if (!isset($row['type'])) return null;

        $type = $row['type'];
        if (!isset($row['params'])) $row['params'] = array();

        try {
            return ShapeFactory::shape($type)->import($row['params']);
        } catch (YellClassNotFoundException $e) {
            return ShapeFactory::shape('null')->import(array('type' => $type));
        }
    }

    private function preProcess()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $_POST['data'] = array(
            array(
                'type' => 'rectangle',
                'params' => array('x' => 0, 'y' => 2)
            ),
            array(
                'type' => 'test',
                'params' => array('test' => 2)
            ),
            array(
                'type' => 'circle',
                'params' => array(
                    'x' => 0,
                    'y' => 34,
                    'radius' => 5,
                    'width' => 2
                )
            ),
            array(
                'type' => 'rectangle'
            ),
            array(
                'type' => 'rectangle',
                'params' => array('x' => 0, 'y' => 2, 'width' => 100, 'height' => 20)
            ),
            array(
                'test' => 'test'
            )
        );
    }

    public function actionIndex()
    {
        $shapePool = new ShapePool();

        /** Симулируем POST-запрос + генерируем набор данных в POST-е */
        $this->preProcess();

        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['data'])) {
            $data = $_POST['data'];

            foreach ($data as $row) {
                /** @var IShape $shape */
                $shape = $this->makeShape($row);
                if (is_null($shape)) continue;
                $shapePool->append($shape);
            }
        }

        echo $this->render('index', array('pool' => $shapePool));
    }
}

