<?php

namespace Yell\Classes;

use Yell\Exceptions\YellClassNotFoundException;

require_once SRC_PATH . 'exceptions' . DS . 'YellClassNotFoundException.class.php';

class Autoload
{
    public static function autoload($className)
    {
        $parts = explode('\\', $className);

        $rootName = array_shift($parts);

        if ($rootName === 'Yell') {
            $path = SRC_PATH;
            $class = array_pop($parts);

            if (!empty($parts)) {
                foreach ($parts as $part) {
                    $path .= strtolower($part) . DS;
                }
            }

            $path .= $class . '.class.php';

            if (is_readable($path)) {
                require_once $path;
            } else {
                throw new YellClassNotFoundException("Class \"{$className}\" not found");
            }
        }
    }
}