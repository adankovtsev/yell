<?php

namespace Yell\Classes;

abstract class Controller
{
    public $layout = 'index';

    public static function create()
    {
        return new static();
    }

    public function render($viewFile, $params = array())
    {
        $output = $this->renderFile($viewFile, $params);

        ob_start();
        ob_implicit_flush();
        extract(array('content' => $output));
        require SRC_PATH . DS . 'views' . DS . 'layout' . DS . $this->layout . '.php';
        $output = ob_get_clean();
        return $output;
    }

    public function renderFile($viewFile, $params = array())
    {
        $className = get_class($this);
        $classPath = explode('\\', $className);
        $className = end($classPath);
        $className = str_replace('controller', '', strtolower($className));

        ob_start();
        ob_implicit_flush();
        extract($params);
        require SRC_PATH . DS . 'views' . DS . $className . DS . $viewFile . '.php';
        $output = ob_get_clean();
        return $output;
    }
}

