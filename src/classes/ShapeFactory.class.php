<?php

namespace Yell\Classes;

use Yell\Interfaces\IShape;

class ShapeFactory
{
    /**
     * @param $shape
     * @return IShape
     */
    public static function shape($shape)
    {
        $className = 'Yell\\Shape\\Shape' . ucfirst($shape);
        return $className::create();
    }
}